module gitlab.com/metakeule/losungen

go 1.21

require (
	gitlab.com/golang-utils/config/v2 v2.3.5
	gitlab.com/golang-utils/fmtdate v1.0.0
)

require (
	github.com/atotto/clipboard v0.1.4 // indirect
	github.com/aymanbagabas/go-osc52/v2 v2.0.1 // indirect
	github.com/charmbracelet/bubbles v0.16.1 // indirect
	github.com/charmbracelet/bubbletea v0.24.1 // indirect
	github.com/charmbracelet/lipgloss v0.7.1 // indirect
	github.com/containerd/console v1.0.4-0.20230313162750-1ae8d489ac81 // indirect
	github.com/emersion/go-appdir v1.1.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	github.com/mattn/go-localereader v0.0.1 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/muesli/ansi v0.0.0-20211018074035-2e021307bc4b // indirect
	github.com/muesli/cancelreader v0.2.2 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.15.1 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/zs5460/art v0.2.0 // indirect
	gitlab.com/golang-utils/dialog v0.0.10 // indirect
	gitlab.com/golang-utils/errors v0.0.2 // indirect
	gitlab.com/golang-utils/scaffold v1.7.2 // indirect
	gitlab.com/golang-utils/version v1.0.1 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	golang.org/x/term v0.6.0 // indirect
	golang.org/x/text v0.12.0 // indirect
)
