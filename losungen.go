package main

import (
	"bytes"
	"fmt"
	"html"
	"io"
	"io/ioutil"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/golang-utils/fmtdate"
	"golang.org/x/text/encoding/charmap"
)

func LosungForDateFile(path string, t time.Time) (losung *Text, lehrtext *Text, err error) {
	ext := strings.ToLower(filepath.Ext(filepath.Base(path)))

	var oldformat bool

	if ext == ".dat" {
		oldformat = true
	}

	var f file
	f.oldformat = oldformat
	err = f.ReadFile(path)
	if err != nil {
		return nil, nil, err
	}

	if f.oldformat {
		return f.getOld(t.YearDay())
	}

	return f.getNew(t)
}

func LosungForDate(oldFormat bool, t time.Time) (losung *Text, lehrtext *Text, err error) {
	year := fmtdate.Format("YYYY", t)
	yeari, err := strconv.Atoi(year)
	if err != nil {
		return nil, nil, err
	}
	var f file
	f.oldformat = oldFormat
	err = f.Read(yeari)
	if err != nil {
		return nil, nil, err
	}

	if f.oldformat {
		return f.getOld(t.YearDay())
	}

	return f.getNew(t)
}

type Text struct {
	Preface  string
	Citation string
	Source   string
	Link     string
}

func (t Text) String() string {
	var pre string
	if t.Preface != "" {
		pre = t.Preface + "\n"
	}
	str := pre + `"` + t.Citation + `"` + "\n" + "(" + t.Source + ")"

	if t.Link != "" {
		str += "\n[" + t.Link + "]"
	}
	return str
}

type file struct {
	file      io.ReadSeeker // fs.File
	oldformat bool
}

func (f *file) ReadFile(filename string) (err error) {
	bt, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}

	f.file = bytes.NewReader(bt)
	return nil
}

// year must have 4 digits
func (f *file) Read(year int) (err error) {
	if year < 1000 {
		return fmt.Errorf("year must have 4 digits")
	}

	if year > 9999 {
		return fmt.Errorf("year must have 4 digits")
	}

	filename := "data/losung%v.csv"

	if f.oldformat {
		filename = "data/losung%v.dat"
	}

	name := fmt.Sprintf(filename, year)
	bt, err := losungen.ReadFile(name)
	if err != nil {
		return err
	}

	f.file = bytes.NewReader(bt)
	return nil
}

func (f *file) readString(from, length int) (string, error) {
	_, err := f.file.Seek(int64(from), 0)
	if err != nil {
		return "", err
	}

	var bf = make([]byte, length)
	n, err := f.file.Read(bf)
	if n != length {
		return "", fmt.Errorf("can't read %v bytes at position %v\n", length, from)
	}

	return string(bf), nil
}

func (f *file) extracText(src []string) Text {
	var t Text
	t.Preface = src[0]
	t.Citation = src[1]
	t.Source = src[2]
	t.Link = src[3]
	return t
}

type columnNew int

var (
	ColDatum        columnNew = 0
	ColWtag         columnNew = 1
	ColSonntag      columnNew = 2
	ColLosungsvers  columnNew = 3
	ColLosungstext  columnNew = 4
	ColLehrtextvers columnNew = 5
	ColLehrtext     columnNew = 6
)

func (f *file) convertTextNew(original string) string {
	dec := charmap.ISO8859_15.NewDecoder()
	asUTF8, err := dec.String(original)
	if err != nil {
		panic("can't decode ISO8859_15 text")
	}
	return strings.TrimSpace(asUTF8)
}

func (f *file) getNew(date time.Time) (losung *Text, lehrtext *Text, err error) {
	datestr := fmtdate.Format("DD.MM.YYYY", date)

	all, err := ioutil.ReadAll(f.file)
	if err != nil {
		return nil, nil, err
	}

	asStr := string(all)

	lines := strings.Split(asStr, "\n")

	if len(lines) == 0 {
		return nil, nil, fmt.Errorf("no lines found\n")
	}

	for _, line := range lines[1:] {
		cols := strings.Split(line, "\t")
		_ = cols
		if strings.TrimSpace(cols[ColDatum]) == datestr {
			losung = &Text{}
			lehrtext = &Text{}
			losung.Citation = f.convertTextNew(cols[ColLosungstext])
			losung.Source = f.convertTextNew(cols[ColLosungsvers])
			lehrtext.Citation = f.convertTextNew(cols[ColLehrtext])
			lehrtext.Source = f.convertTextNew(cols[ColLehrtextvers])
			return
		}
	}
	return nil, nil, fmt.Errorf("konnte keine Losung für das Datum finden\n")
}

func (f *file) getOld(dayOfYear int) (losung *Text, lehrtext *Text, err error) {
	posLen, err := f.readString((dayOfYear*12)-12, 12)

	if err != nil {
		return nil, nil, err
	}

	pos := strings.TrimSpace(posLen[:6])
	length := strings.TrimSpace(posLen[6:])

	posi, err := strconv.Atoi(pos)

	if err != nil {
		return nil, nil, err
	}

	lengthi, err := strconv.Atoi(length)

	if err != nil {
		return nil, nil, err
	}

	rawtext, err := f.readString(posi-1, lengthi+1)

	if err != nil {
		return nil, nil, err
	}

	rawtext = html.UnescapeString(rawtext)
	parts := strings.Split(rawtext, "\xa7")
	los := f.extracText(parts[:4])
	lehr := f.extracText(parts[4:8])
	return &los, &lehr, nil
}
