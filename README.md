# Die Losungen der Evangelischen Brüder-Unität – Herrnhuter Brüdergemeinde

Als Kommandozeilenprogramm.

## Installation (Go ist erforderlich):

    go install gitlab.com/metakeule/losungen

## Verwendung

    losungen

Gibt die Losung des aktuellen Tages aus.

    losungen 2023.08.15
    
Gibt die Losung für den 15. August 2023 aus.

Zur Zeit sind nur die Jahre 2022 und 2023 vorhanden.

Aber aktuelle CSV-Dateien können hier heruntergeladen werden:
https://www.losungen.de/digital/daten

Und dann so verwendet werden:

    losungen --datei=losung2023.csv
    
Dann muss natürlich das Jahr der Datei zum Datum passen.

## Copyright

(c) Evangelische Brüder-Unität – Herrnhuter Brüdergemeinde (https://www.herrnhuter.de).
Weitere Informationen finden Sie hier: https://www.losungen.de

## Quellen

- https://www.losungen.de/digital/daten
- https://www.losungen.de/fileadmin/media-losungen/download/Losungen_Quellenverzeichnis_2019.pdf
- https://www.combib.de/losungphp/index.html