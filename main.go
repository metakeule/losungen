package main

import (
	"fmt"

	"os"
	"time"

	"gitlab.com/golang-utils/config/v2"
)

var (
	cfg = config.New("losungen", 0, 0, 2, "Die Losungen der Evangelischen Brüder-Unität – Herrnhuter Brüdergemeinde",
		config.AsciiArt("Losungen"),
	)
	dateArg = cfg.LastDate("datum", "Muss im Format JJJJ-MM-TT angegeben werden. Gibt die Losung für das angegebene Datum zurück. (Wenn kein Datum angegeben wurde, wird die Losung des heutigen Tages zurückgegeben.)")
	//oldFormatArg = cfg.Bool("altes-format", "Dateien im alten Format laden")
	fileArg = cfg.String("datei", "Angegebene Datei als Quelle der Losungen nutzen.")
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, err.Error())
		os.Exit(1)
	}

	os.Exit(0)
}

func run() error {
	err := cfg.Run()

	if err != nil {
		fmt.Println("Verwendung")
		fmt.Println(cfg.Usage())
		os.Exit(1)
	}

	var date = time.Now()

	if dateArg.IsSet() {
		date = dateArg.Get()
	}

	var losung *Text
	var lehrtext *Text

	if fileArg.IsSet() {
		losung, lehrtext, err = LosungForDateFile(fileArg.Get(), date)
	} else {
		losung, lehrtext, err = LosungForDate(false, date)
		//losung, lehrtext, err := LosungForDate(oldFormatArg.Get(), date)
	}

	if err != nil {
		return err
	}

	fmt.Printf("%s\n\n%s\n", losung, lehrtext)
	return nil
}
